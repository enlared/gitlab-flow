# Introducción a GitLab Flow
## Estrategias de ramas o flujos de trabajo:
-	Se dan en un flujo de trabajo de desarrollo de software en el contexto de git
-	Describe como los desarrolladores van a crear, colaborar, y fusionar sus ramas de código fuente en el código base
-	Habilita el desarrollo concurrente en el código base

## Workflows
Git provee una amplia variedad de estrategias y flujos de trabajo. Algunas de ellas, son las siguientes:
* Git flow
* GitHub flow
* GitLab Flow
   - Environment branches
   - Release branches



## GitLab Flow con environment branches
<img src="images/1-gitlab-flow.png" width=600 align=center>

## GitLab Flow con release branches
<img src="images/2-gitlab-flow-release-branches.png" width=600 align=center>


### En resumen de GitLab Flow

* Environment branches
  - Los cambios se promueven a través de una o más ramas de pre-producción
* Release branches
  - Se utiliza cuando se lanza software al público. Por ejemplo, proyectos open source, o algún tipo de software descargable

****

### Principios

   - Cree una nueva branch local para la tarea y realice **push periódicamente a una branch con el mismo nombre en el servidor**
   - Cuando finalice la tarea, **solicite una solicitud de fusión (merge request)** para la branch *master*
   - Cuando se revisaron/aprobaron (reviewed/approved) los cambios enviados, combínelos (merge) con la branch master
   - Una vez en el master, el código **debe ser integrado en las ramas de los ambientes internos de la empresa**, hasta llegar a la rama de producción.
   - Al fusionarse con el master, **elimine la rama** donde se desarrolló la tarea dejando el repositorio más organizado. 

### Ventajas

   - Este flujo garantiza un estado limpio en las ramas en cualquier punto del ciclo de vida del proyecto.
   - Define cómo hacer Integración Continua (CI) y Entrega Continua (CD)
   - Es muy flexible según las decisiones del equipo.
   - Es menos complejo que GitFlow Workflow

### Desventajas

   - Es más complejo que GitHub Workflow
   - El historial de Git se vuelve ilegible debido a las diversas solicitudes de combinación (merge) entre ramas.

### Flujo de GitLab frente a flujo de GitHub

La mayor diferencia entre GitLab Flow y GitHub Flow son las ramas del entorno en GitLab Flow (por ejemplo, preproducción y producción). El flujo de GitHub asume que si está en el master, puede pasar a producción. El flujo de GitLab permite que el código pase por entornos internos antes de llegar a producción.

### Flujo de GitLab frente a GitFlow

El flujo de GitLab es más favorable para la aplicación de la integración continua que Gitfow. La división master/develop hace que la Entrega Continua y la Integración Continua sean más difíciles con Gitflow. En GitFlow, la creación de ramas como hotfix y release puede hacer que la integración sea compleja.

**** 
## Como nombrar las branches

Tipos de ramas:
* **feature**: Se utiliza para las nuevas características
* **bugfix**: Se utiliza para la corrección del fallos
* **release**: Se utiliza para la creacion de nuevas versiones
* **hotfix**: Se utiliza para la corrección de fallos en producción

Para la creación especificamos primero el `tipo` de rama, seguido por una  `/` y luego el `nombre` de la rama.

Un ejemplo como deberiamos crear una **feature** llamada **nueva-caracteristica** seria la siguiente:
```
git checkout -b feature/nueva-caracteristica
```
****

## Flujo de GitLab en la práctica  

### Flujo de mejora


El flujo comienza con la rama **master**, la rama del entorno de **pre-production** y la rama del entorno de **production**. Todas estas ramas deben estar protegidas, ya que los desarrolladores no realizarán commits directamente sobre ellas.


<img src="images/flow1.png" width=600 align=center>

De la misma manera que en el flujo de GitHub, para iniciar una nueva demanda de desarrollo, debe crear una rama específica para esta demanda y periódicamente realizar push para la rama del mismo nombre al repositorio remoto.

<img src="images/flow2.png" width=600 align=center>

Al finalizar la demanda, se solicita una *solicitud de fusión* (merge request) para el master. Se puede abrir una revisión de código (code review) en GitLab y se puede iniciar una discusión sobre el cambio

<img src="images/flow3.png" width=600 align=center>

Una vez aprobado, el cambio debe integrarse en la rama master. 

<img src="images/flow4.png" width=600 align=center>

A continuación, se debe realizar una fusión (merge) entre la rama **master** y la rama del entorno de preproducción. Se debe ejecutar el pipeline para compilar el proyecto y ejecutar las pruebas automatizadas.

<img src="images/flow5.png" width=600 align=center>

Al pasar las pruebas automatizadas, se debe realizar una nueva fusión (merge) a la rama de producción desde la rama pre-producction. Se vuelve a ejecutar el pipeline para ejecutar las pruebas automatizadas una vez más e implementar los cambios en producción.

Se debe crear una etiqueta para marcar una versión estable del sistema y se debe eliminar la rama de características para que el repositorio esté más organizado.

<img src="images/flow6.png" width=600 align=center>

****

### Flujo de revisión (hotfix)

En caso de un cambio urgente que no puede esperar a que el master se fusione en producción. Es posible crear una rama para realizar la tarea urgente desde la rama de producción. Este sería un flujo equivalente a la rama "hotfix" de GitFlow.

Por lo general, estos cambios urgentes son correcciones de errores, y se hacen pocos commits.

Comencemos con un repositorio donde hay 2 tareas asignadas a la rama master, pero que aún no están disponibles en producción. De repente, surge un error que hay que corregir en producción lo antes posible.

<img src="images/flow7.png" width=600 align=center>

Cree una rama de corrección de errores desde la rama de producción.

<img src="images/flow8.png" width=600 align=center>

Empuje (push) la rama al repositorio remoto.

<img src="images/flow9.png" width=600 align=center>

Solicite una *Solicitud de fusión* (merge request) para la rama master. Por lo general, como en el flujo de mejora.



<img src="images/flow10.png" width=600 align=center>

Espere a que se apruebe la *solicitud de fusión* y se apruebe la compilación de la rama master, lo que indica que no hay errores en su confirmación.


<img src="images/flow11.png" width=600 align=center>


Cuando pasen las pruebas automáticas en la rama master, por urgencia, no se debe esperar a que la rama master se fusione con las ramas de otros entornos. Realice una *solicitud de fusión  / fusión* (merge request/merge) **desde su rama de caracteristica** (feature branch) a todas las demás ramas de entornos internos.

Como su rama de características es la rama de producción con su nueva confirmación, generalmente solo se fusionará una confirmación única con la rama de producción. Asegúrese de fusionar estos cambios en todas las ramas de entornos. O se puede perder la corrección de errores/cambio urgente.

Cree una etiqueta, para marcar una nueva versión del sistema.


<img src="images/flow12.png" width=600 align=center>

Solo que ahora elimine la rama de corrección de errores. Las ramas de mejora permanecen en el repositorio y su corrección está en todos los entornos del sistema.

<img src="images/flow13.png" width=600 align=center>

Cuando se completen el resto de cambios, las mejoras estarán disponibles en producción sin borrar la corrección de errores, ya que ya estaba integrada en la rama master del proyecto.

Se marca una etiqueta de versión con mejoras.


<img src="images/flow14.png" width=600 align=center>

Al final del ciclo, todos los cambios se publican en producción. Se eliminan todas las ramas no permanentes. El flujo termina y comienza un nuevo ciclo.



<img src="images/flow15.png" width=600 align=center>

****

### Flujo de revisión alternativo  (hotfix)

Alternativamente, para tareas urgentes, puede usar el comando cherry-pick para obtener solo un commit específico y enviarlo a los otros entornos del sistema. Entonces, por ejemplo, puede enviar solo un commit específico a producción mientras los demás aún están esperando que finalicen las pruebas manuales, en la rama de preproducción.


<img src="images/flow16.png" width=600 align=center>

****

# Tipos de commits

<img src="images/merge.png" width=600 align=center>

****
## Aclaraciones
Una cosa importante en este flujo es que los commits siempre deben integrarse desde el master hasta la producción. Nunca al revés. Como se dijo en la documentación de flujo original de GitLab: **"Este flujo de trabajo, donde las commits solo fluyen hacia abajo, garantiza que todo se pruebe en todos los entornos"** ("This workflow, where commits only flow downstream, ensures that everything is tested in all environments. ").

Máster -> Preproducción -> Producción (OK)

Producción -> Preproducción -> Máster (ERROR)

A pesar del nombre, el flujo de GitLab no es un flujo que se aplique solo en gitlab.com. Se puede usar con cualquier repositorio que use Git.

****

## Referencias:
* [Introduction to GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
* [GitLab Tutorials](https://www.youtube.com/playlist?list=PLrSqqHFS8XPaZ99jWakexkbMhwwNFYvIA)
* [GitLab CI Tutorial Series](https://www.youtube.com/playlist?list=PLZMWkkQEwOPmGolqJPsAm_4fcBDDc2to_)
* [GitLab Flow](https://github.com/jadsonjs/gitlab-flow)
